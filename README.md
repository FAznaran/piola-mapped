This is the repository containing the exact code used for the numerical experiments in the paper [_Transformations for Piola-mapped elements_](https://doi.org/10.5802/smai-jcm.91), as well as scripts for the associated plots. It also contains the coarse mesh used in the example from [Li (2018)](https://www-users.cse.umn.edu/~arnold/papers/LiThesis.pdf). 

The exact software used to run these scripts can be installed from Zenodo at [https://doi.org/10.5281/zenodo.5596313](https://doi.org/10.5281/zenodo.5596313).

First, activate the Firedrake venv using the above instructions. 
Then, install the tabulate function in the venv using   
**python3 -m pip install tabulate**

To run the MTW examples, do   
**sh mtw\_examples.sh**   
in the mtw directory.
To run the AW examples, do   
**sh aw\_examples.sh**   
in the aw directory.

To generate the coarse mesh from the given .geo file, do   
**gmsh -2 -clscale 0.15 LiHoleCoarse.geo**   
in the aw directory, using gmsh version 4.8.4.
Images were generated using Paraview version 5.7.0.

For further information, please contact <aznaran@maths.ox.ac.uk>.
