#!/bin/bash
## 21st Oct 2021
## Script to perform the AW examples in
## "Transformations for Piola-mapped elements".
## This assumes that the Firedrake venv is already
## activated.

export OMP_NUM_THREADS=1

echo -e "AW MMSs:\n"

python3 H-R_2D_MMS.py --conf "False"
python3 H-R_2D_MMS.py --conf "False" --nu 0.4999999

## AWc MMS
python3 H-R_2D_MMS.py --uex "[ -exp(sin(pi*y/2)), 3*cos(pi*x)]" --sigex "A_inv(epsilon(uex))"
python3 H-R_2D_MMS.py --uex "[ -exp(sin(pi*y/2)), 3*cos(pi*x)]" --sigex "A_inv(epsilon(uex))" --nu 0.4999999

echo -e "Li example:\n"

## AWc and the vertex star

## Note that to view this solution, WarpByVector should be scaled by 0.3 in Paraview.
python3 li2018.py --gamma 100 --alpha 1

python3 li2018.py --gamma 1000 --alpha 1
python3 li2018.py --gamma 10000 --alpha 1
python3 li2018.py --gamma 100000 --alpha 1

## Save the associated plot locally
python3 nitsche_plot.py
mv nitsche.txt AWc_nitsche.txt
mv nitsche.pdf AWc_nitsche.pdf

## With AWnc and LU
python3 li2018.py --conf "False" --params "lu" --gamma 100 --alpha 0
python3 li2018.py --conf "False" --params "lu" --gamma 1000 --alpha 0
python3 li2018.py --conf "False" --params "lu" --gamma 10000 --alpha 0
python3 li2018.py --conf "False" --params "lu" --gamma 100000 --alpha 0

python3 nitsche_plot.py
mv nitsche.txt AWnc_nitsche.txt
mv nitsche.pdf AWnc_nitsche.pdf
