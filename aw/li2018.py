from firedrake import *
from firedrake.cython import dmcommon
import numpy
import argparse
import os.path

green = '\033[92m'
white = '\033[0m'
blue = '\033[94m'

parser = argparse.ArgumentParser(add_help = False)
parser.add_argument("--gamma", type = float, default = 100)
parser.add_argument("--alpha", type = float, default = 1.0)
parser.add_argument("--conf", type = str, default = "True")
parser.add_argument("--params", type = str, default = "fsmg")
args, _ = parser.parse_known_args()

print(blue + "Nitsche parameter: ", args.gamma, white)
print(green + "AL parameter: ", args.alpha, white)

# Evaluation of a constant compliance tensor
# (in the homogeneous isotropic case)
def A(sig):
    return (1/(2*mu))*(sig - nu*tr(sig)*I)

# Deviator
def dev(sig):
    return sig - 0.5*(sig[0,0] + sig[1,1])*I

lu = {"snes_type": "ksponly",
          "snes_linesearch_type": "basic",
          "snes_max_it": 100,
          "snes_monitor": None,
          "snes_converged_reason": None,
          "ksp_type": "preonly",
          "pc_type": "lu",
          "pc_factor_mat_solver_type": "mumps",
          "mat_mumps_icntl_14": "1000",
          "mat_type": "aij",
          "snes_stol": 0.0,}

fslu = {
       "mat_type": "matfree",
       "snes_monitor": None,
       "snes_type": "ksponly",
       "ksp_type": "fgmres",
       "ksp_monitor_true_residual": None,
       "ksp_rtol": 1.0e-9,
       "ksp_atol": 1.0e-9,
       "pc_type": "fieldsplit",
       "pc_fieldsplit_type": "schur",
       "pc_fieldsplit_schur_factorization_type": "full",
       "pc_fieldsplit_schur_precondition": "user",
       "fieldsplit_0": {
           "ksp_type": "preonly",
           "pc_type": "python",
           "pc_python_type": "firedrake.AssembledPC",
           "assembled_pc_type": "lu",
           "assembled_pc_factor_mat_solver_type": "mumps",
           "assembled_mat_mumps_icntl_14": "1000",
           },
       "fieldsplit_1": {
           "ksp_type": "gmres",
           "ksp_max_it": "1",
           "pc_type": "python",
           "pc_python_type": "__main__.DGMassInv",
           }
       }

fsmg = {        
        "mat_type": "aij",
        "snes_monitor": None,
        "snes_type": "ksponly",
        "ksp_type": "gmres",
        "ksp_monitor_true_residual": None,
        "ksp_rtol": 1.0e-9,
        "ksp_atol": 1.0e-9,
        "pc_type": "fieldsplit",
        "pc_fieldsplit_type": "schur",
        "pc_fieldsplit_schur_factorization_type": "full",
        "pc_fieldsplit_schur_precondition": "user",
        "fieldsplit_0": {
            "ksp_type": "richardson",
            "ksp_norm_type": "unpreconditioned",
            "ksp_max_it": 1,
            "pc_type": "mg",
            "pc_mg_type": "full",
            "mg_coarse_pc_type": "python",
            "mg_coarse_pc_python_type": "firedrake.AssembledPC",
            "mg_coarse_assembled": {
                "mat_type": "aij",
                "pc_type": "lu",
                "pc_factor_mat_solver_type": "mumps",
                "mat_mumps_icntl_14": "1000",
                },
            "mg_levels": {
                "ksp_type": "chebyshev",
                "ksp_norm_type": "unpreconditioned",
                "ksp_max_it": 3,
                "ksp_convergence_test": "skip",
                "pc_type": "python",
                "pc_python_type": "firedrake.ASMStarPC",
                "pc_star_construct_dim": 0,
                "pc_star_backend": "tinyasm",
                },
            },
        "fieldsplit_1": {
            "ksp_type": "richardson",
            "ksp_richardson_self_scale": None,
            "ksp_max_it": "1",
            "pc_type": "python",
            "pc_python_type": "__main__.DGMassInv",
            }
        }

params = eval(args.params)

class DGMassInv(PCBase):

    def initialize(self, pc):
        _, P = pc.getOperators()
        appctx = self.get_appctx(pc)
        V = dmhooks.get_function_space(pc.getDM())
        u = TrialFunction(V)
        v = TestFunction(V)
        massinv = assemble(Tensor(inner(u, v)*dx).inv)
        self.massinv = massinv.petscmat

    def update(self, pc):
        pass

    def apply(self, pc, x, y):
        self.massinv.mult(x, y)
        y.scale(-1 * float(alpha))

    def applyTranspose(self, pc, x, y):
        raise NotImplementedError("Sorry!")

dist_params = {"partition": True, "overlap_type": (DistributedMeshOverlapType.VERTEX, 1)}

mesh = Mesh("LiHoleCoarse.msh", distribution_parameters = dist_params)
#Colour the mesh
LEFT = 1
RIGHT = 2
TOP = 3
BOTTOM = 4
HOLES = 5

dm = mesh.topology_dm
sec = dm.getCoordinateSection()
coords = dm.getCoordinatesLocal()
dm.removeLabel(dmcommon.FACE_SETS_LABEL)
dm.createLabel(dmcommon.FACE_SETS_LABEL)

faces = dm.getStratumIS("exterior_facets", 1).indices

for face in faces:
    vertices = dm.vecGetClosure(sec, coords, face).reshape(2, 2)

    if numpy.allclose(vertices[:, 0], 0):
        dm.setLabelValue(dmcommon.FACE_SETS_LABEL, face, LEFT)
    elif numpy.allclose(vertices[:, 0], 3):
        dm.setLabelValue(dmcommon.FACE_SETS_LABEL, face, RIGHT)
    elif numpy.allclose(vertices[:, 1], 0):
        dm.setLabelValue(dmcommon.FACE_SETS_LABEL, face, BOTTOM)
    elif numpy.allclose(vertices[:, 1], 1):
        dm.setLabelValue(dmcommon.FACE_SETS_LABEL, face, TOP)
    else:
        dm.setLabelValue(dmcommon.FACE_SETS_LABEL, face, HOLES)

ds_stressfree = ds(TOP) + ds(BOTTOM) + ds(HOLES)

# levels, not including the coarsest mesh (on which we don't solve).
levels = 5
mh = MeshHierarchy(mesh, levels, distribution_parameters = dist_params)

if os.path.exists("nitsche.txt"):
    command = "a"
else:
    command = "w"

errors = open("nitsche.txt", command)

if eval(args.conf):
    el = "AWc"
    deg = 3
else:
    el = "AWnc"
    deg = 2

print(el)

for i, mesh in enumerate(mh[1:]):
    stress_ele = FiniteElement(el, mesh.ufl_cell(), deg)
    disp_ele = VectorElement("DG", mesh.ufl_cell(), 1)
    ele = MixedElement([stress_ele, disp_ele])
    Z = FunctionSpace(mesh, ele)
    print("Degrees of freedom: %s %s" % (f"{Z.dim():,}", [f"{W.dim():,}" for W in Z]), flush = True)

    z = Function(Z)
    (sigma, u) = split(z)

    nu = Constant(0.2)
    E = Constant(10)

    lam = (E*nu) / ((1+nu) * (1 - 2*nu))
    mu = lam*(1 - 2*nu)/(2*nu)

    I = Identity(mesh.geometric_dimension())

    (x, y) = SpatialCoordinate(mesh)

    u_left = Constant((0, 0))
    u_right = Constant((-1.0, 0))

    n = FacetNormal(mesh)

    # Nitsche stuff
    gamma = Constant(args.gamma)
    h = CellDiameter(mesh)
    f = Constant((0, 0))
    g = Constant((0, 0))

    # Augmented Lagrangian stuff
    alpha = Constant(args.alpha)

    ex = -1

    J = (
          0.5 * inner(A(sigma), sigma)*dx
        + inner(div(sigma) - f, u)*dx
        + inner(alpha*(div(sigma) - f), div(sigma) - f)*dx
        - inner(dot(sigma, n), u_left)*ds(LEFT)
        - inner(dot(sigma, n), u_right)*ds(RIGHT)
        - inner(dot(sigma, n) - g, u)*ds_stressfree
        + ((h**ex)*gamma/2)*inner(dot(sigma, n) - g, dot(sigma, n) - g)*ds_stressfree
        )

    F = derivative(J, z, TestFunction(Z))

    solve(F == 0, z, solver_parameters=params)

    (sigma, u) = z.split()

    traction_resid = sqrt(assemble(inner(dot(sigma, n) - g, dot(sigma, n) - g)*ds_stressfree))
    print(blue + "Traction residual in the L2 norm: ", traction_resid, white, "\n")

    h = project(h, FunctionSpace(mesh, "DG", 0))
    h = h.at(0, 0)
    errors.write(str(h) + ' ' + str(traction_resid) + "\n")

    norm_dev = project(sqrt(inner(dev(sigma), dev(sigma))), FunctionSpace(mesh, "DG", 3))
    sigma.rename("Stress")
    norm_dev.rename("Norm of the shear stress")
    u.rename("Displacement")
    File("output/output_{num}.pvd".format(num = str(i))).write(sigma, norm_dev, u)

errors.close()

#Citations.print_at_exit()
