### Hellinger-Reissner MMS for the conforming and nonconforming
### Arnold-Winther elements.

from firedrake import *
from firedrake.petsc import PETSc
import numpy
from tabulate import tabulate
import argparse

parser = argparse.ArgumentParser(add_help = False)
parser.add_argument("--mu", type = float, default = 1.0)
parser.add_argument("--nu", type = float, default = 0.25)
parser.add_argument("--uex", type = str, default = "[sin(pi*x)*sin(pi*y), sin(pi*x)*sin(pi*y)]")
parser.add_argument("--sigex", type = str, default = "[[cos(pi*x)*cos(3*pi*y), y + 2*cos(pi*x/2)], [y + 2*cos(pi*x/2), -sin(3*pi*x)*cos(2*pi*x)]]")
parser.add_argument("--Mesh", type = str, default = "UnitSquareMesh(N_base, N_base)")
parser.add_argument("--warp", type = str, default = "True")
parser.add_argument("--conf", type = str, default = "True")
args, _ = parser.parse_known_args()

green = '\033[92m'
white = '\033[0m'
print(green + "nu =", args.nu, ", uex =" , args.uex, ", sigex =", args.sigex, white)

if eval(args.conf):
    el = "AWc"
    deg = 3
else:
    el = "AWnc"
    deg = 2

print(el)

N_base = 2
mesh = eval(args.Mesh)
levels = 6
mh = MeshHierarchy(mesh, levels - 1)
if eval(args.warp):
    print("Warping mesh.")
    V = FunctionSpace(mesh, mesh.coordinates.ufl_element())
    eps = Constant(1 / 2**(N_base - 1))
    x, y = SpatialCoordinate(mesh)
    new = Function(V).interpolate(as_vector([x + eps*sin(2*pi*x)*sin(2*pi*y),
                                             y - eps*sin(2*pi*x)*sin(2*pi*y)]))
    coords = [new]
    for mesh in mh[1:]:
        fine = Function(mesh.coordinates.function_space())
        prolong(new, fine)
        coords.append(fine)
        new = fine
    for mesh, coord in zip(mh, coords):
        mesh.coordinates.assign(coord)

# Poisson ratio and first Lame constant; 
# second Lame constant
mu = Constant(args.mu)
nu = Constant(args.nu)
lam = 2*args.mu*args.nu/(1 - 2*args.nu)
print(green + "lam =", lam, white)
lam = Constant(lam)

I = Identity(2)

# Evaluation of a constant compliance tensor
# (in the homogeneous isotropic case)
def A(sig):
    return (1/(2*mu))*(sig - nu*tr(sig)*I)

# and its inverse
def A_inv(G):
    sig2 = 2*mu*G[0,1]
    sig3 = 2*mu*G[1,0]
    sig1 = (2*mu/(1 - 2*nu))*((1 - nu)*G[0,0] + nu*G[1,1])
    sig4 = (2*mu/(1 - 2*nu))*(nu*G[0,0] + (1 - nu)*G[1,1])
    return as_tensor([[sig1, sig2],
                      [sig3, sig4]])

# Linearised strain tensor
def epsilon(u):
    return 0.5*(grad(u) + grad(u).T)

# Experimental Order of Convergence
def EOC(h1, h2, E1, E2):
    return (numpy.log(E1) - numpy.log(E2))/(numpy.log(h1) - numpy.log(h2))

errors = []
results = []
StressElement = FiniteElement(el, triangle, deg)
element = MixedElement([StressElement, VectorElement("DG", mesh.ufl_cell(), 1)])
for i, msh in enumerate(mh):
    N = N_base * 2**i
    x, y = SpatialCoordinate(msh)
    uex = as_vector(eval(args.uex))
 
    sigex = as_tensor(eval(args.sigex))
     
    # Compliance and constraint residuals of the MMS
    comp_r = A(sigex) - epsilon(uex)
    cons_r = div(sigex)
      
    V = FunctionSpace(msh, element)

    Uh = Function(V)

    (sigh, uh) = split(Uh)
    (tau, v) = TestFunctions(V)
      
    n = FacetNormal(msh)

    # H-R residual, incorporating MMS residuals
    F = (
          inner(A(sigh), tau)*dx
        + inner(div(tau), uh)*dx
        + inner(div(sigh), v)*dx
        - inner(comp_r, tau)*dx
        - inner(cons_r, v)*dx
        - inner(dot(tau, n), uex)*ds
        )

    params = {"snes_type": "newtonls",
              "snes_linesearch_type": "basic",
              "snes_max_it": 100,
              "ksp_type": "preonly",
              "pc_type": "lu",
              "pc_factor_mat_solver_type": "mumps",
              "mat_type": "aij",
              "snes_rtol": 1e-5,
              "snes_atol": 1e-25,
              "snes_stol": 0.0,}

    solve(F == 0, Uh, solver_parameters = params)
     
    error_disp = sqrt(assemble(inner(uex - uh, uex - uh)*dx))
    error_stress = sqrt(assemble(inner(sigh - sigex, sigh - sigex)*dx))
    error_div_stress = sqrt(assemble(inner(div(sigh - sigex), div(sigh - sigex))*dx))
        
    h = 1 / N
    if i == 0:
        EOC_disp, EOC_stress, EOC_div_stress = 0, 0, 0
    else:
        h_prev = 2*h
        N_prev = N/2
        prev_disp = errors[i - 1][1]
        prev_stress = errors[i - 1][3]
        prev_div_stress = errors[i - 1][5]
        EOC_disp = EOC(h, h_prev, error_disp, prev_disp)
        EOC_stress = EOC(h, h_prev, error_stress, prev_stress)
        EOC_div_stress = EOC(h, h_prev, error_div_stress, prev_div_stress)
        
    errors.append([N, error_disp, GREEN % (EOC_disp), error_stress, GREEN % (EOC_stress), error_div_stress, GREEN % (EOC_div_stress)])

print("\n", tabulate(errors, headers = ['N', 'Disp error', GREEN % ('Disp EOC'), 'Stress error', GREEN % ('Stress EOC'), 'div(stress) error', GREEN % ('div(stress) EOC')]), "\n")

#Citations.print_at_exit()
