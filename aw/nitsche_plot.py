
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

# Convert a small float value, to the TeX of a truncated version of
# it in scientific notation.
def sci(N):
    N = "{:.2e}".format(N)
    lead = N[:4]
    exp = N[-3:]
    exp = str(int(exp))
    return lead+"$\\times 10^{" + exp + "}$"

# Number of Nitsche parameters
num = 4
# Number of levels in the hierarchy (not including
# the coarsest mesh, on which we don't solve).
levels = 5

H = []
Err = [[0 for j in range(levels)] for i in range(num)]

i = 0
j = 0
for line in open("nitsche.txt", "r"):
    values = [float(s) for s in line.split()]
    if (i == 0):
        H.append(values[0])

    Err[i][j] = values[1]
    if (j == levels - 1):
        i += 1
        j = -1

    j += 1

plt.rc('text', usetex = True)
plt.rc('font', family = 'serif')
plt.rc('text.latex', preamble = r'\usepackage{amsmath}' + r'\usepackage{amssymb}')

fig, ax = plt.subplots(figsize = (6.5, 6.75))

# So that the O(h), O(h^2) lines appear just above the rest
ratio = Err[0][0] / H[0]
ratio_square = ratio / H[0]
ax.plot(H, [2*ratio*h for h in H], label = "$\mathcal{O}(h)$", linestyle = "dashed")
ax.plot(H, [2*ratio_square*h*h for h in H], label = "$\mathcal{O}(h^2)$", linestyle = "dashed")

markers=["*", "o", "v", "^", "s"]
# Power of 10 at which we begin the gammas.
begin = 2
for i in range(num):
    exp = str(i+begin)
    ax.plot(H, Err[i][:], label="$\gamma = 10^"+exp+"$", marker=markers[i])

ax.set_xscale('log', base = 10)
ax.set_yscale('log', base = 10)
ax.set_xlabel(r'$h$')
ax.set_ylabel(r'$\|\sigma_h\mathbf{n}\|_{L^2(\Gamma_N;\mathbb{R}^2)}$')

ax.set_xticks(H)
ax.tick_params(axis = 'x', which = 'minor', bottom = 'False')
ax.set_xticklabels([sci(h) for h in H])

ax.xaxis.set_minor_locator(ticker.NullLocator())
ax.tick_params(axis = 'both', labelsize = 14)
axes = plt.gca()
axes.xaxis.label.set_size(20)
axes.yaxis.label.set_size(20)

plt.legend(loc = 'lower right', prop = {'size': 15})

plt.tight_layout()

fig.savefig("nitsche.pdf")
