#!/bin/bash
# 20th Oct 2021
# Script to perform the MTW examples in the paper
# "Transformations for Piola-mapped elements".
# This assumes that the Firedrake venv is already
# activated.

export OMP_NUM_THREADS=1

echo -e "MTW MMS:\n"
python3 vary_epsilon.py

echo -e "MTW elasticity example:\n"
python3 planar_elasticity.py --nu 0.3
python3 planar_elasticity.py --nu 0.45
python3 planar_elasticity.py --nu 0.49
python3 planar_elasticity.py --nu 0.4999
python3 planar_elasticity.py --nu 0.4999999
