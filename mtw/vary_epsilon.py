# Varying epsilon in the canonical saddle point problem which
# motivated the invention of MTW.
# (Obtained by editing our MTW test for Firedrake.)
from firedrake import *
import numpy as np
from tabulate import tabulate
import matplotlib.pyplot as plt

convergence_orders = lambda x: np.log2(np.array(x)[:-1] / np.array(x)[1:])

N_base = 3
msh = UnitSquareMesh(N_base, N_base)
levels = 6
mh = MeshHierarchy(msh, levels - 1)

# Warp base mesh
V = FunctionSpace(msh, msh.coordinates.ufl_element())
eps = Constant(1 / 2**(N_base - 1))
x, y = SpatialCoordinate(msh)
new = Function(V).interpolate(as_vector([x + eps*sin(2*pi*x)*sin(2*pi*y),
                                         y - eps*sin(2*pi*x)*sin(2*pi*y)]))

# and propagate to refined meshes
coords = [new]
for msh in mh[1:]:
    fine = Function(msh.coordinates.function_space())
    prolong(new, fine)
    coords.append(fine)
    new = fine

for msh, coord in zip(mh, coords):
    msh.coordinates.assign(coord)

# To obtain a picture of the mesh in Paraview
# (Figure 7 in the paper), set 'Representation'
# to 'Wireframe', 'Line Width' as 2, and
# background color as white.
mesh_to_plot = mh[0]
fake_DG = FunctionSpace(mesh_to_plot, "DG", 0)
fake_u = Function(fake_DG)
File("output/MMS_mesh_picture.pvd").write(fake_u)

params = {"snes_type": "newtonls",
          "snes_linesearch_type": "basic",
          "mat_type": "aij",
          "snes_max_it": 10,
          "snes_lag_jacobian": -2,
          "snes_lag_preconditioner": -2,
          "ksp_type": "preonly",
          "pc_type": "lu",
          "pc_factor_shift_type": "inblocks",
          "snes_rtol": 1e-16,
          "snes_atol": 1e-25}

vary = 7

Eps = [2**(-2*k) for k in range(vary - 1)]
Eps.append(0)

u_table = []
p_table = []

for eps in Eps:
    u_errors = []
    p_errors = []
    u_EOCs = []
    p_EOCs = []
    eps = Constant(eps)
    for i, msh in enumerate(mh):
        N = N_base * 2**i
        h = 1 / N
        x, y = SpatialCoordinate(msh)
        pex = cos(pi*x)*cos(2*pi*y)
        uex = as_vector([2**(1 - y), Constant(0)])

        V = FunctionSpace(msh, "MTW", 3)
        W = FunctionSpace(msh, "DG", 0)
        Z = V*W

        up = Function(Z)
        u, p = split(up)
        v, w = TestFunctions(Z)

        n = FacetNormal(msh)
        
        uex_proj = project(uex, V)
        bcs = [DirichletBC(Z.sub(0), as_vector([uex_proj[0], uex_proj[1]]), (1, 2, 3))]

        F = (
                inner(u, v)*dx 
                + inner((eps**2)*grad(u), grad(v))*dx
                - inner(p, div(v))*dx
                + inner(div(u), w)*dx 
                - inner(uex, v)*dx
                + inner((eps**2)*div(grad(uex)), v)*dx
                - inner(grad(pex), v)*dx
                - inner(div(uex), w)*dx
                - inner(dot((eps**2)*grad(uex), n) - pex*n, v)*ds(4)
                )

        solve(F == 0, up, bcs = bcs, solver_parameters = params)

        u, p = up.split()
        u_err = errornorm(uex, u)
        p_err = errornorm(pex, p)
        u_errors.append(u_err)
        p_errors.append(p_err)
        
    u_EOC = convergence_orders(u_errors)
    p_EOC = convergence_orders(p_errors)
    average_u_EOC = sum(u_EOC) / (levels - 1)
    average_p_EOC = sum(p_EOC) / (levels - 1)
    u_errors.append(GREEN % (average_u_EOC))
    p_errors.append(BLUE % (average_p_EOC))
    u_errors.insert(0, eps)
    p_errors.insert(0, eps)
    u_table.append(u_errors)
    p_table.append(p_errors)

range_of_N = ['2^' + str(i) for i in range(levels)]

print("velocity errors:\n", tabulate(u_table, ["eps \ N"] + range_of_N + ["EOC"]), "\n")
print("pressure errors:\n", tabulate(p_table, ["eps \ N"] + range_of_N + ["EOC"]), "\n")

#Citations.print_at_exit()
