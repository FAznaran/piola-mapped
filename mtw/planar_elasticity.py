# Testing the star relaxation with the
# MTW element for primal linear elasticity.
import petsc4py.PETSc
from firedrake import *
import argparse

white = '\033[0m'
blue = '\033[94m'

parser = argparse.ArgumentParser(add_help = False)
parser.add_argument("--nu", type = float, default = 0.3)
args, _ = parser.parse_known_args()

params = {"snes_type": "ksponly",
          "ksp_type": "cg",
          "ksp_monitor_true_residual": None,
          "ksp_norm_type": "unpreconditioned",
          "mat_type": "matfree",
          "pc_type": "mg",
          "mg_coarse": {
              "ksp_type": "preonly",
              "pc_type": "python",
              "pc_python_type": "firedrake.AssembledPC",
              "assembled_pc_type": "lu",
              "factor_mat_solver_type": "mumps"
          },
          "mg_levels": {
              "ksp_type": "chebyshev",
              "ksp_max_it": 2,
              "ksp_convergence_test": "skip",
              "pc_type": "python",
              "pc_python_type": "firedrake.PatchPC",
              "patch_pc_patch_save_operators": True,
              "patch_pc_patch_partition_of_unity": False,
              "patch_pc_patch_construct_type": "star",
              "patch_pc_patch_construct_dim": 0,
              "patch_pc_patch_sub_mat_type": "seqdense",
              "patch_sub_ksp_type": "preonly",
              "patch_sub_pc_type": "lu",
          }
}

L = 25
H = 1
Nx = 125
Ny = 5
base = RectangleMesh(Nx, Ny, L, H, diagonal = 'crossed')
levels = 4
mh = MeshHierarchy(base, levels - 1)
mesh = mh[-1]

def eps(v):
    return sym(grad(v))

mu = Constant(38000)
nu = Constant(args.nu)
print(blue + "nu =", args.nu, white)

E = 2*mu*(1 + nu)
lmbda = 2*mu*nu/(1 - 2*nu)
lmbda = 2*mu*lmbda/(lmbda + 2*mu)

def sigma(v):
    return lmbda*tr(eps(v))*Identity(2) + 2.0*mu*eps(v)

rho_g = 1e-3
f = Constant((0, -rho_g))

V = FunctionSpace(mesh, "MTW", 3)
v = TestFunction(V)
u = Function(V, name = "Displacement")

print("Degrees of freedom: %s" % (f"{V.dim():,}"), flush = True)

gg = Function(V)
bc = DirichletBC(V, gg, (1,))

F = inner(sigma(u), eps(v))*dx - inner(f, v)*dx
    
problem = NonlinearVariationalProblem(F, u, bc)
solver = NonlinearVariationalSolver(problem, solver_parameters = params)
tm = TransferManager(use_averaging = True)
solver.set_transfer_manager(tm)
solver.solve()

uu = project(u, VectorFunctionSpace(mesh, "DG", 3))

print("Maximal deflection:", -uu((L - 0.001, H/2.))[1])
print("Beam theory deflection:", float(3*rho_g*L**4/2/E/H**3), "\n")

u = u*(1e3)
uu = project(u, VectorFunctionSpace(mesh, "DG", 3))
File("output/beam_{nu}.pvd".format(nu = args.nu)).write(uu)

#Citations.print_at_exit()
